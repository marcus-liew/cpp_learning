#include <iostream>
#include "./head/Animal.h"

using namespace std;

int main()
{
    Animal dog;
    dog.set_animal_data("DaHei", 23.3, 42.5, "black");
    dog.run();
    dog.get_animal_data();
}