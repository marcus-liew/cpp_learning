#include <iostream>
#include <string>
using namespace std;

class Animal
{
private:
    /* data */
    string name;
    float weight;
    float height;
    string color;

public:
    Animal(/* None Args */);
    void set_animal_data(string name, float weight, float height, string color);
    void get_animal_data();
    void run();

    ~Animal();
};