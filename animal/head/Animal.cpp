#include <iostream>
#include <string>
#include "Animal.h"

using namespace std;

Animal::Animal() { cout << "* --- invoke constractor --- *" << endl; }

Animal::~Animal() { cout << "* --- invoke ~ --- *" << endl; }

void Animal::set_animal_data(string name, float weight, float height, string color)
{
    this->name = name;
    this->weight = weight;
    this->height = height;
    this->color = color;
}

void Animal::get_animal_data()
{
    cout << "name is: " << this->name << endl;
    cout << "weight is: " << this->weight << endl;
    cout << "height is:" << this->height << endl;
    cout << "color is: " << this->color << endl;
}

void Animal::run()
{
    cout << this->name << " is running." << endl;
}
